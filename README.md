## Python Minimax Path Solver
Takes in a weighted undirected graph, and finds the path that minimizes the max weight of its edges. In other words, solves the [Minimax path problem](https://en.wikipedia.org/wiki/Widest_path_problem) in O(E lg V) time by applying Kruskal's and BFS.

### The problem
[Minimax path problem](https://en.wikipedia.org/wiki/Widest_path_problem) (Wikipedia).

A carrier compary is located in a city of a mountaineous country, where he delivers calibrated precision instruments elsewhere in the country. If the instruments are taken to a too high of an altitude, they have to be calibrated again. This is why, when accepting a delivery job, the carrier company wants to know the best path to their destination - the path which minimizes the maximum altitude of its roads.

The entirety of possible routes can be represented as a weighted undirected graph - nodes are cities, edges the roads between them. Edges are weighted by their highest altitude. Cities are marked as numbers, and the city with the number 1 is always the starting point.

![ExampleGraph](./img/example_graph.PNG)

*Example graph. The best route from city 1 to city 7 is 1 -> 2 -> 4-> 7 with the altitude of 78.*

This graph is first read from a text file. The first line has the number of cities and roads (nodes and edges). The following lines represent each road as three numbers: starting city, destination city, altitude of the road. Last line of the file has the destination city as a number.

The previous graph looks like this in text format:

![ExampleGraphTextFormat](./img/example_graph_text_format.PNG)

The path finding has to happen in a reasonable amount of time for a graph with 100 cities.

### Solution
Find the minimum spanning tree, or MST, of the graph. The MST connects all nodes together, without cycles and with the minimum possible total edge weight. The unique path we're looking for is easy to find from the MST with either BFS or DFS.

### Time complexity
Let V be the number of nodes (or V for vertices) and E the number of edges in the beginning.

Kruskal's algorithm does O(V) make_set operations, as well as O(E) find_set and union operations. Python's sort() method has time complexity of O(E lg E), so we get O(E lg V) as the time complexity of this implementation of Kruskal's.

The BFS implementation's time complexity is O(V + E). However, it receives a minimum spanning tree from Kruskal's, which means that E = V - 1. Thus, we get an upper limit for the running time: V + V - 1 = 2V - 1, which is less than 2V => final time complexity for BFS is O(V).

Together, Kruskal's and BFS give us O(E lg V) + O(V) = **O(E lg V)**.

---

### Instructions
1. Clone the repository
2. Navigate to the folder and run the program with `python main.py`
3. Program asks for a text file.
4. Input path to one of the included files or your own, e.g `/testdata_large/graph_ADS2018_200.txt` (.txt file ext. is required!)
5. Program finds and outputs the best path and running time.