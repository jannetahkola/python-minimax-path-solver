"""Define WeightedGraph class."""

class WeightedGraph:
    """Data structure for an undirected weighted graph data."""

    def __init__(self, vertices_n):
        """
        Initialize graph.

        Args:
            vertices_n (int): Number of vertices in the graph.
        """
        # array of vertices, e.g. [vertice1, vertice2]
        self.vertices = [0] * vertices_n
        # list of edges and their weights, e.g. [(edge weight, start vertice, end vertice)]
        self.edges = []
        # dictionary which holds edge weights with the edges as keys, e.g. {(start vertice, end vertice): weight}
        self.weights_by_edges = {}
        # dictionary which holds neighboring vertices for each vertice, e.g. {vertice: [neighbor1, neighbor2]}
        self.neighbors = {}
        # initialization
        for i in range(vertices_n):
            current_vertice = (i + 1)
            self.vertices[i] = current_vertice
            self.neighbors[current_vertice] = []

    def insert_weighted_edge(self, s, e, w):
        """
        Insert a weighted edge into the graph.

        Args:
            s (int): Start vertice.
            e (int): End vertice.
            w (int): Edge weight.
        """
        self.edges.append((w, s, e))
        self.weights_by_edges[(s, e)] = w
        self.neighbors[s].append(e)
        self.neighbors[e].append(s)

    def print_weighted_graph(self):
        """Print out the graph."""
        print("Weighted Graph:")
        print("Vertices = " + str(self.vertices))
        print("Edges = " + str(self.edges))
        print("Weights by edges = " + str(self.weights_by_edges))
        print("Neighbors = " + str(self.neighbors))