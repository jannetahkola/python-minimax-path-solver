"""Define Kruskal's algorithm."""
from weighted_graph import WeightedGraph

sets_list = []

def make_set(vertice):
    """
    Create a new set containing the given vertice.

    Args:
        vertice (int): Vertice to base the set on.
    """
    sets_list.append({vertice})

def find_set(vertice):
    """
    Find the set that contains the given vertice.

    Args:
        vertice (int): Vertice to find the set by.

    Returns:
        Set containing the vertice.

    """
    # O(n)
    for current_set in sets_list:
        if vertice in current_set:
            return current_set

def union(v1, v2):
    """
    Find and merge sets that contain the param vertices and delete them afterwards.

    Args:
        v1 (int): Vertice to find the first set by.
        v2 (int): Vertice to find the second set by.
    """
    set1 = find_set(v1)
    set2 = find_set(v2)
    if set1 != set2:
        sets_list.append(set1 | set2)
        sets_list.remove(set1)
        sets_list.remove(set2)

def mst_kruskal(graph):
    """
    Create a minimum spanning tree from the given weighted graph using Kruskal's algorithm. Time complexity is O(E lg V).

    Args:
        graph (WeightedGraph): Graph which the algorithm is executed on.

    Returns:
        A new WeightedGraph object representing the initial graph's MST.

    """
    mst_graph = WeightedGraph(len(graph.vertices))
    # O(V) make-set operations
    for vertice in graph.vertices:
        make_set(vertice)
    edges = graph.edges
    # sort edges by weight, O(E lg E) according to Google
    edges.sort()
    # O(E) find-set and union operations
    for edge in edges:
        w, s, e = edge
        if find_set(s) != find_set(e):
            union(s, e)
            mst_graph.insert_weighted_edge(s, e, w)
    return mst_graph