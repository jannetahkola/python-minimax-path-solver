"""Define breadth-first search algorithm."""
import math
INF = math.inf
from queue import Queue

p = {}
best_path = []

def path_bfs(g, s, e):
    """
    Execute breadth-first search for given graph g using s as the starting point and e as the destination. Time complexity is O(V + E).

    Args:
        g (WeightedGraph): Weighted graph.
        s (int): Start vertice.
        e (int): Destination vertice.
    """
    color = {}
    d = {}
    # O(V) to initialize collections
    for u in g.vertices:
        color[u] = "W"
        d[u] = INF
        p[u] = None
    color[s] = "G"
    d[s] = 0
    p[s] = None
    Q = Queue(len(g.vertices))
    Q.enqueue(s)
    # O(E) iterations at most
    while not Q.empty():
        u = Q.dequeue()
        for v in g.neighbors[u]:
            if color[v] == "W":
                color[v] = "G"
                d[v] = d[u] + 1
                p[v] = u
                Q.enqueue(v)
        color[u] = "B"
    # create best path and print results
    trace_best_path(g, s, e)
    if len(best_path) > 0:
        print(f"Best path from {s} to {e} is \n{best_path}")
        print_best_path_altitude(g.weights_by_edges)

def print_best_path_altitude(edges):
    """
    Print out the highest altitude of the best path.

    Args:
        edges (dict): Dictionary in the format {(start node, end node): weight}.
    """
    path_altitude = 0
    for i in range(len(best_path)):
        if i < len(best_path) - 1:
            s = best_path[i]
            e = best_path[i + 1]
            if (s, e) in edges:
                if edges[(s, e)] > path_altitude:
                    path_altitude = edges[(s, e)]
            else:
                if edges[(e, s)] > path_altitude:
                    path_altitude = edges[(e, s)]
    print(f"with an altitude of {path_altitude}.")

def trace_best_path(g, s, e):
    """
    Trace the best path from s to e in graph g and insert the vertices of the path to a list.

    Args:
        g (WeightedGraph): Graph to analyze.
        s (int): Start vertice.
        e (int): Destination vertice .
    """
    if s == e:
        best_path.append(s)
    else:
        if p[e] == None:
            print(f"No path from {s} to {e} exists.")
            best_path.clear()
        else:
            trace_best_path(g, s, p[e])
            best_path.append(e)