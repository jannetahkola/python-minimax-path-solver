"""Define queue."""

class Queue:
    """Data structure for a FIFO queue."""

    def __init__(self, size):
        """Initialize queue."""
        self.slots = [0]*size
        self.head = 0
        self.tail = 0
        self.num_taken = 0
        self.size = size

    def empty(self):
        """
        Check if the queue is empty.
        
        Returns:
            True if the queue is empty, False otherwise.

        """
        return self.num_taken == 0
    
    def full(self):
        """
        Check if the queue is full.

        Returns:
            True if the queue is full, False otherwise.

        """
        return self.num_taken == self.size

    def enqueue(self, x):
        """
        Insert the parameter element into the queue if not full.

        Args:
            x (int): Element to insert.

        Returns:
            True if the insert operation succeeded, False otherwise.

        """
        if self.full():
            return False
        self.slots[self.tail] = x
        self.tail = (self.tail + 1) % self.size
        self.num_taken += 1
        return True

    def dequeue(self):
        """
        Get the next value in the queue if not empty.
        
        Returns:
            Next value in the queue if it exists, -1 otherwise.
            
        """
        if self.empty():
            return -1
        retval = self.slots[self.head]
        self.head = (self.head + 1) % self.size
        self.num_taken -= 1
        return retval