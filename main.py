"""Main program."""
import time
from weighted_graph import WeightedGraph
from kruskal import mst_kruskal
from bfs import path_bfs

def process_file(filename):
    """
    Parse a text file.

    Args:
        filename (string): File to parse.
    
    Returns:
        Number of cities and roads (int), destination node (int) and the roads and their weights (string[]).

    """
    try:
        with open(filename, "r") as file:
            # read individual lines to a string array
            lines_str = [line.rstrip('\n') for line in file]
            # parse number of cities to an int
            cities_n = int(lines_str[0].split(" ")[0])
            # parse number of roads to an int
            roads_n = int(lines_str[0].split(" ")[1])
            # parse destination to an int
            dest = int(lines_str[roads_n + 1].split(" ")[0])
            # remove first and last lines from the original string array
            # to leave only roads with start, end and weight
            del lines_str[0]
            del lines_str[roads_n]
    except OSError:
        raise
    return lines_str, cities_n, roads_n, dest

def main():
    """Read a file containing a road network and execute bfs and Kruskal's on it to find the best path."""
    print("-----")
    print("Import a text file:")
    print("(Give a filename or path to a file, filename must include file extension .txt, e.g. road_network.txt)")
    filename = input("> ")

    start = 1

    try:
        roads_arr, cities_n, roads_n, dest = process_file(filename)
    except OSError as e:
        print(f"Couldn't open file '{filename}': {e.args}")
        return

    # create initial graph and populate it with edges
    graph = WeightedGraph(cities_n)
    for i in roads_arr:
        s, e, w = map(int, i.split(" "))
        graph.insert_weighted_edge(s, e, w)

    #graph.print_weighted_graph()

    print("-----")
    print(f"Looking for the best path from {start} to {dest}...")
    print("-----")

    # start timer
    start_time = time.perf_counter()

    # find MST of the initial graph, create a new graph from it
    mst_graph = mst_kruskal(graph)

    # find and print the best path
    path_bfs(mst_graph, start, dest)

    # end counter
    end_time = time.perf_counter() - start_time
    print("-----")
    print(f"Finished in {end_time} seconds.")
    print("-----")

main()